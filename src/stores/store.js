import { defineStore } from "pinia";

export const useMovieStore = defineStore("movie", {
  state: () => ({
    listMovie: JSON.parse(localStorage.getItem("listMovie")) || [],
  }),
  actions: {
    addList(movie) {
      this.listMovie.push(movie);
      localStorage.setItem("listMovie", JSON.stringify(this.listMovie));
    },
    deleteMovie(id) {
      this.listMovie = this.listMovie.filter((value) => value.id !== id);
      localStorage.setItem("listMovie", JSON.stringify(this.listMovie));
    },
    updateMovie(movie) {
      const index = this.listMovie.findIndex((value) => value.id === movie.id);
      this.listMovie[index] = movie;
      localStorage.setItem("listMovie", JSON.stringify(this.listMovie));
    },
    setListMovie(listMovie) {
      this.listMovie = listMovie;
      localStorage.setItem("listMovie", JSON.stringify(this.listMovie));
    },
  },
  getters: {
    getMovie: (state) => (id) =>
      state.listMovie.find((value) => value.id === id),
    getCounter: (state) => (genre) =>
      genre === "Total"
        ? state.listMovie.length
        : state.listMovie.filter((value) =>
            value.genre.some((value) => value === genre)
          ).length,
  },
});
