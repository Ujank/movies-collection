import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", {
  state: () => ({
    counter: 0,
  }),
  getters: {
    doubleCount: (state) => (multiplier) => {
      console.log(multiplier);
      return state.counter + multiplier;
    },
  },
  actions: {
    increment() {
      this.counter++;
    },
  },
});
